(function($){

	// main slider
	if ($('.js-main-slider').length) {
		$('.js-main-slider').bxSlider({});
	}

	// clients slider
	if ($('.js-main-slider').length) {
		$('.js-clients-slider').bxSlider({
			pager:false,
			minSlides: 4,
	  		maxSlides: 4,
	  		slideWidth: 235,
	  		slideMargin: 5,
	  		auto: true,
		});
	}

//    SCScripts

    $(document).ready(function(){
        $('.lkmenuitem').click(function(){
            $('.lkmenuitem').removeClass('active');
            $(this).addClass('active');
        });
        $('.lkpaginationitem').click(function(){
            $('.lkpaginationitem').removeClass('active');
            $(this).addClass('active');
        });
        $('.lk_optiononoff').click(function(){
            $('.lk_option').toggleClass('disable');
        });
        $('.lkfirst_par').click(function(){
            $('.lkresultchild').hide();
            $('.first_result').show();
        });
        $('.lksecond_par').click(function(){
            $('.lkresultchild').hide();
            $('.second_result').show();
        });
        $('.lkthird_par').click(function(){
            $('.lkresultchild').hide();
            $('.third_result').show();
        });
				$('.lkfourth_par').click(function(){
            $('.lkresultchild').hide();
            $('.fourth_result').show();
        });
				$('.lkfifth_par').click(function(){
            $('.lkresultchild').hide();
            $('.fifth_result').show();
        });
				$('.lksixth_par').click(function(){
            $('.lkresultchild').hide();
            $('.sixth_result').show();
        });

				// Замена цены при переключении табов
				function changePrice(price) {
					$('.tarification-row .opt').removeClass('active');
					$('#tariffName').html(price[0].name);
					$('.bundlePrice').html(price[0].firstPrice);
					$('.abonentPrice').html(price[0].secondPrice);
					$('.js-multiTabs').removeClass('active');
					$('.js-multiTabs:eq(0)').addClass('active');
					$('.right-side-options.current .right-side-option').removeClass('active');
					$('.right-side-options.current .right-side-option:eq(0)').addClass('active');
					activePrice = price;
				}

				$('.tarification-row .opt:eq(0)').click(function() {
					changePrice(lightPrice);
					$('.right-side-options').removeClass('current');
					$('.right-side-options:eq(0)').addClass('current');
					$(this).addClass('active');
				});
				$('.tarification-row .opt:eq(1)').click(function() {
					changePrice(optimumPrice);
					$('.right-side-options').removeClass('current');
					$('.right-side-options:eq(1)').addClass('current');
					$(this).addClass('active');
				});
				$('.tarification-row .opt:eq(2)').click(function() {
					changePrice(premiumPrice);
					$('.right-side-options').removeClass('current');
					$('.right-side-options:eq(2)').addClass('current');
					$(this).addClass('active');
				});

				var tabCount = $('.js-multiTabs').length
				$('.js-multiTabs').click(function() {
					$('.js-multiTabs').removeClass('active');
					$(this).addClass('active');
					for (var i=0; i<tabCount; i++) {
						if ($('.js-multiTabs:eq('+i+')').hasClass('active')) {
							$('.right-side-options.current .right-side-option').removeClass('active');
							$('.right-side-options.current .right-side-option:eq('+i+')').addClass('active');
							$('.bundlePrice').html(activePrice[i].firstPrice);
							$('.abonentPrice').html(activePrice[i].secondPrice);
						}
					}
				});

				var lightPrice = [{
					name: 'тариф «light»',
					firstPrice: '17 000',
					secondPrice: '1000'
				},{
					name: 'тариф «light»',
					firstPrice: '15 000',
					secondPrice: '1000'
				}];
				var optimumPrice = [{
					name: 'тариф «optimum»',
					firstPrice: '21 600',
					secondPrice: '1300'
				},{
					name: 'тариф «optimum»',
					firstPrice: '19 000',
					secondPrice: '1300'
				}];
				var premiumPrice = [{
					name: 'тариф «premium»',
					firstPrice: '29 600',
					secondPrice: '1500'
				},{
					name: 'тариф «premium»',
					firstPrice: '25 000',
					secondPrice: '1500'
				}];
				var garagePrice = {
					name: 'Гараж',
					firstPrice: '15 000',
					secondPrice: '1500'
				};
				var cottagePrice = [{
					name: 'беспроводной тариф',
					firstPrice: '35 600',
					secondPrice: '2000'
				},{
					name: 'стандартный тариф',
					firstPrice: '29 000',
					secondPrice: '2000'
				}];

				var activePrice;
				if ($(".variation-row .opt:eq(0)").hasClass('active')) {
					activePrice = lightPrice;
				} else if ($(".variation-row .opt:eq(1)").hasClass('active')) {
					activePrice = cottagePrice;
				}
    })

		$('.lk-btn').click(function(e){
			e.preventDefault();
			$(this).toggleClass('active');
		}).on('click', '.pop', function(e) {
			e.stopPropagation();
		})

})(jQuery);
